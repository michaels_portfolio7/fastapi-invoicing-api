-- Invoice Info --




CREATE TABLE IF NOT EXISTS `client` (
    `id` INT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (`id`),
    `cust_gvr_number` INT,
    `cust_companyname` VARCHAR(50),
    `cust_address_streetname` VARCHAR(50),
    `cust_address_streetnumber` INT,
    `cust_address_zipcode` INT,
    `cust_address_city` VARCHAR(50),
    `cust_address_state` VARCHAR(50),
    `cust_address_country` VARCHAR(50),
    `cust_vat_number` VARCHAR(50),
    `cust_regist_number` INT,
    `cust_regit_state` VARCHAR(50),
    `cust_nace_code` INT,
    `cust_signatory1_fname` VARCHAR(50),
    `cust_signatory1_lname` VARCHAR(50),
    `cust_signatory2_fname`  VARCHAR(50),
    `cust_signatory2_lname` VARCHAR(50),
    `cust_admin_fname` VARCHAR(50),
    `cust_admin_lname` VARCHAR(50),
    `cust_email` VARCHAR(50),
    `cust_phone` INT,
    `cust_bank_name` VARCHAR(50),
    `cust_bic_num` INT,
    `cust_iban_num` INT,
    `cust_vat_countries` VARCHAR(50),
    `cust_contract_engagement` VARCHAR(50),
    `cust_contract_poa` VARCHAR(50)
);


CREATE TABLE IF NOT EXISTS `vendors`(
    `id` INT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (`id`),
    `vat_number` VARCHAR(50),
    `vat_country` VARCHAR(50),
    `legal_name` VARCHAR(50),
    `registration_number` INT,
    `company_status` VARCHAR(50),
    `origin_country` VARCHAR(50),
    `incorp_date` DATE,
    `entity_type` VARCHAR(50),
    `SIC_codes` INT,
    `vendor_address_streetname` VARCHAR(50),
    `vendor_address_streetnumber` INT,
    `vendor_address_city` VARCHAR(50),
    `vendor_address_zipcode` INT,
    `vendor_address_state` VARCHAR(50),
    `vendor_address_country` VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS `invoice_info`(
    `id` INT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (`id`),
    `inv_gvr_code` VARCHAR(50),
    `inv_status` VARCHAR(50),
    `inv_issue_date` DATE,
    `inv_invoice_no` INT(50),
    `vat_sum` INT(50),
    `inv_item_title` VARCHAR(50),
    `inv_item_amount` FLOAT,
    `inv_item_tax` FLOAT,
    `inv_tax_code` INT(50),
    `inv_net_amount`  FLOAT,
    `inv_gross_amount` FLOAT,
    `inv_vat_amount`  FLOAT,
    `inv_current` VARCHAR(50),
    `inv_taxcode_name` VARCHAR(50),
   ` inv_taxcode_taxvaluepercentage` INT(5),
    `inv_taxcode_localtaxrate`  INT(5),
    `inv_file_original` VARCHAR(50),
    `inv_file_new` VARCHAR(50),
    `inv_owner_id` INT,   
    
    INDEX `client_inv` (`inv_owner_id`),
    FOREIGN KEY (`inv_owner_id`)
        REFERENCES `client`(`id`)
);

CREATE TABLE IF NOT EXISTS `user_info`(
    `id` INT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (`id`),
    `inv_gvr_code` VARCHAR(50),
    `inv_status` VARCHAR(50),
    `inv_issue_date` DATE,
    `inv_invoice_no` INT(50),
    `vat_sum` INT(50),
    `inv_item_title` VARCHAR(50),
    `inv_item_amount` FLOAT,
    `inv_item_tax` FLOAT,
    `inv_tax_code` INT(50),
    `inv_net_amount`  FLOAT,
    `inv_gross_amount` FLOAT,
    `inv_vat_amount`  FLOAT,
    `inv_current` VARCHAR(50),
    `inv_taxcode_name` VARCHAR(50),
   ` inv_taxcode_taxvaluepercentage` INT(5),
    `inv_taxcode_localtaxrate`  INT(5),
    `inv_file_original` VARCHAR(50),
    `inv_file_new` VARCHAR(50),
    `inv_owner_id` INT,

    INDEX `client_inv` (`inv_owner_id`),
    FOREIGN KEY (`inv_owner_id`)
        REFERENCES `client`(`id`)
);

INSERT INTO `vendors` (`id`, `vat_number`, `vat_country`, `legal_name`, `registration_number`, `company_status`, `origin_country`, `incorp_date`, `entity_type`, `SIC_codes`, `vendor_address_streetname`, `vendor_address_streetnumber`, `vendor_address_city`, `vendor_address_zipcode`, `vendor_address_state`, `vendor_address_country`)
VALUES 
(NULL, 'AT-U12345678', 'Austria', 'Austria Company GmBh', '1234567890', 'Active', 'Austria', '2012-12-12', 'LLC', '0121', 'Obdacher Bundesstrasse', '39', 'Obergnserndorf', '211', 'Carinthia', 'Austria'),
(NULL, 'dc-U12345678', 'Austria', 'Austria Company GmBh', '1234567890', 'Active', 'Austria', '2012-12-12', 'LLC', '0121', 'Obdacher Bundesstrasse', '39', 'Obergnserndorf', '211', 'Carinthia', 'Austria');


INSERT INTO `invoice_info` (`id`, `inv_gvr_code` `inv_status`, `inv_issue_date`, `inv_invoice_no`, `vat_sum`, `inv_item_title`, `inv_item_amount`, `inv_item_tax`, `inv_tax_code`, `inv_net_amount`, `inv_gross_amount`, `inv_vat_amount`, `inv_current`, `inv_taxcode_name`, ` inv_taxcode_taxvaluepercentage`, `inv_taxcode_localtaxrate`, `inv_file_original`, `inv_file_new`, `inv_owner_id`)
VALUES 
(NULL, 'GVR-123FA' 'Active', '2012-12-12', '123455', '1235', 'Invoice Item Title', '122.12', '123.22', '12344', '123.12', '12.12', '12.12', 'what?', 'Tax code name', '12', '1', '/urlpath/here', '/urlpath/here', NULL);

INSERT INTO `client` (`id`, `cust_gvr_number`, `cust_companyname`, `cust_address_streetname`, `cust_address_streetnumber`, `cust_address_zipcode`, `cust_address_city`, `cust_address_state`, `cust_address_country`, `cust_vat_number`, `cust_regist_number`, `cust_regit_state`, `cust_nace_code`, `cust_signatory1_fname`, `cust_signatory1_lname`, `cust_signatory2_fname`, `cust_signatory2_lname`, `cust_admin_fname`, `cust_admin_lname`, `cust_email`, `cust_phone`, `cust_bank_name`, `cust_bic_num`, `cust_iban_num`, `cust_vat_countries`, `cust_contract_engagement`, `cust_contract_poa`) 
VALUES 
(NULL, '10003', 'Lidl GmBh', 'Alpenstrasse', '34', '4792', 'Steinerzaun', 'Upper Austria', 'Austria', 'AT-U12345678', '3245346', 'Upper Austria', '0912', 'Marc', 'Dumpff', 'John ', 'Doe', 'Larry', 'Joe', 'email@email.com', '0680858518', 'HSBC', '12341234', '234234123', 'Germany, Austrilia', '/urlpath/here', '/urlpath/here');

