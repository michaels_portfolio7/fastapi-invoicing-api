from datetime import timedelta, datetime

import jwt
from database import engine, SessionLocal

from sqlalchemy.orm import Session

from fastapi.security import HTTPBasicCredentials, HTTPBearer
from fastapi import HTTPException, Depends
import crud


security = HTTPBearer()

def get_db():
    db = None
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()

def create_access_token(*, data: dict, expires_delta: timedelta = None):
    secret_key = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
    algorithm = "HS256"
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, secret_key, algorithm=algorithm)
    return encoded_jwt

def has_access(db: Session = Depends(get_db), credentials: HTTPBasicCredentials = Depends(security)):
    secret_key = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
    algorithm = "HS256"

    token = credentials.credentials
    # print("payload => ", credentials)
    # print("token => ", token)

    try:
        payload = jwt.decode(token, secret_key, algorithms=algorithm)
    except jwt.InvalidTokenError:
        raise HTTPException(status_code=400, detail="Your token is wrong, sorry.")
    # print("final => ", payload)
    # print("final => ", payload["sub"])

    db_user = crud.get_user_by_username(db=db, username=payload["sub"])
    if db_user is None:
        raise HTTPException(status_code=400, detail="Username doesn't exist")
    else:
        print("Winner, Winner! Chicken Dinner")


def has_admin_access(db: Session = Depends(get_db), credentials: HTTPBasicCredentials = Depends(security)):
    secret_key = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
    algorithm = "HS256"
    token = credentials.credentials
    # print("payload => ", credentials)
    # print("token => ", token)

    try:
        payload = jwt.decode(token, secret_key, algorithms=algorithm)
    except jwt.InvalidTokenError:
        raise HTTPException(status_code=400, detail="Your token is fucked up, sorry.")
    # print("final => ", payload)
    # print("final => ", payload["sub"])

    db_user = crud.get_user_by_username(db=db, username=payload["sub"])
    if db_user is None:
        raise HTTPException(status_code=400, detail="Username doesn't exist")
    else:
        print("Winner, Winner! Chicken Dinner")
        admin_user = crud.get_user_by_role(role='admin', db=db, username=payload["sub"])
        if admin_user is None:
            raise HTTPException(status_code=400, detail="You're not admin. Login attempt reported.")
        else:
            print("Winner winner, waffle dinner?")
            print(admin_user)