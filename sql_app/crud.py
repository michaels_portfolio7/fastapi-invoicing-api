from sqlalchemy.orm import Session
from models import InvoiceInfo
import bcrypt
from schemas import invoice, client, sch_user
import models


#### CLIENTS
from schemas.client import CreateClient, ShowClient
from models import Client


def create_new_client(client: CreateClient, db : Session):
    client = Client(**client.dict())
    db.add(client)
    db.commit()
    db.refresh(client)
    return client



def client_by_id(id: int, db : Session):
    return db.query(models.Client).filter(models.Client.id == id).first()

def update_client_by_id(id: int, client: ShowClient, db: Session):
    existing_client = db.query(models.Client).filter(models.Client.id == id)
    if not existing_client.first():
        return 0
    client.__dict__.update()
    existing_client.update(client.__dict__)
    db.commit()
    # db.refresh(client)
    return

def delete_client_by_id(id:int, db: Session):
    existing_client = db.query(models.Client).filter(models.Client.id == id)
    if not existing_client.first():
        return 0
    existing_client.delete(synchronize_session=False)
    db.commit()
    return 1


#### VENDORS
from schemas.vendors import CreateVendor, ShowVendor
from models import Vendor


def create_new_vendor(vendor: CreateVendor, db : Session):
    vendor = Vendor(**vendor.dict())
    db.add(vendor)
    db.commit()
    db.refresh(vendor)
    return vendor

def vendor_by_id(id: int, db : Session):
    return db.query(models.Vendor).filter(models.Vendor.id == id).first()

def update_vendor_by_id(id: int, vendor: ShowVendor, db: Session):
    existing_vendor = db.query(models.Vendor).filter(models.Vendor.id == id)
    if not existing_vendor.first():
        return 0
    vendor.__dict__.update()
    existing_vendor.update(vendor.__dict__)
    db.commit()
    # db.refresh(vendor)
    return

def delete_vendor_by_id(id:int, db: Session):
    existing_vendor = db.query(models.Vendor).filter(models.Vendor.id == id)
    if not existing_vendor.first():
        return 0
    existing_vendor.delete(synchronize_session=False)
    db.commit()
    return 1


##### USERS #######
def get_user_by_username(db: Session, username: str):
    return db.query(models.UserInfo).filter(models.UserInfo.username == username).first()

def create_user(db: Session, user: sch_user.UserCreate):
    hashed_password = bcrypt.hashpw(user.password.encode('utf-8'), bcrypt.gensalt())
    db_user = models.UserInfo(username=user.username, password=hashed_password, fullname=user.fullname)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

def check_username_password(db: Session, user: sch_user.UserAuthenticate):
    db_user_info: models.UserInfo = get_user_by_username(db, username=user.username)
    return bcrypt.checkpw(user.password.encode('utf-8'), db_user_info.password.encode('utf-8'))

def user_by_id(id: int, db : Session):
    return db.query(models.UserInfo).filter(models.UserInfo.id == id).first()

def update_user_by_id(id: int, user: sch_user.UserInfo, db: Session):
    existing_user = db.query(models.UserInfo).filter(models.UserInfo.id == id)
    if not existing_user.first():
        return 0
    user.__dict__.update()
    existing_user.update(user.__dict__)
    db.commit()
    # db.refresh(vendor)
    return

def delete_user_by_id(id:int, db: Session):
    existing_user = db.query(models.UserInfo).filter(models.UserInfo.id == id)
    if not existing_user.first():
        return 0
    existing_user.delete(synchronize_session=False)
    db.commit()
    return

##### INVOICES #######

from schemas.invoice import CreateInvoice, ShowInvoice
from models import InvoiceInfo
from typing import List


def get_all_invoices( db : Session, skip: int = 0, limit: int = 100,) -> List[models.InvoiceInfo]:
    return db.query(models.InvoiceInfo).offset(skip).limit(limit).all()

def get_all_inv(db : Session):
    return db.query(models.InvoiceInfo).all()


def create_new_invoice(invoice: CreateInvoice, db : Session):
    invoice = InvoiceInfo(**invoice.dict())
    db.add(invoice)
    db.commit()
    db.refresh(invoice)
    return invoice

def create_new_invoice2(clientid: int, invoiceid: int, filename:str, db : Session ):
    # file = InvoiceInfo.inv_file_original

    new_invoice = models.InvoiceInfo(inv_file_original=filename, inv_owner_id=clientid, inv_gvr_code=invoiceid)

    db.add(new_invoice)
    db.commit()
    db.refresh(new_invoice)
    return new_invoice


def invoice_by_id(id: int, db : Session):
    return db.query(models.InvoiceInfo).filter(models.InvoiceInfo.id == id).first()



def update_invoice_by_id(id: int, invoice: ShowInvoice, db: Session):
    existing_invoice = db.query(models.InvoiceInfo).filter(models.InvoiceInfo.id == id)
    if not existing_invoice.first():
        return 0
    invoice.__dict__.update()
    existing_invoice.update(invoice.__dict__)
    db.commit()
    # db.refresh(client)
    return

def delete_invoice_by_id(id:int, db: Session):
    existing_invoice = db.query(models.InvoiceInfo).filter(models.InvoiceInfo.id == id)
    if not existing_invoice.first():
        return 0
    existing_invoice.delete(synchronize_session=False)
    db.commit()
    return 1



##### INVOICES #######
