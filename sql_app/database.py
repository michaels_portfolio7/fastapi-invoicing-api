
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

#8091 is PHPMyAdmin
# SQLALCHEMY_DATABASE_URL = "mysql+mysqlconnector://root:XXXXXXX@XXXXXXX/api_db"

SQLALCHEMY_DATABASE_URL = "mysql+mysqlconnector://root:root@localhost:8889/gvr_api"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL,
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# For model.py files
Base = declarative_base()
