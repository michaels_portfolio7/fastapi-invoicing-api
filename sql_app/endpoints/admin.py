#Admin Route /admin

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
import models
from database import engine, SessionLocal


router = APIRouter()

def get_db():
    db = None
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


@router.get("admin")
async def say_hi(name: str):
    return "Hi "

