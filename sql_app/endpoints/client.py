# CLIENT
###########
from schemas.client import CreateClient, ShowClient

import crud
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
import models
from database import engine, SessionLocal

router = APIRouter()

def get_db():
    db = None
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()



@router.get('/clients/all', tags=["Client"])
def all_clients(db: Session = Depends(get_db)):
    return db.query(models.Client).all()

@router.post('/create-client', response_model=ShowClient, tags=["Client"])
def create_client(client : CreateClient,  db: Session = Depends(get_db)):
    client = crud.create_new_client(client=client, db=db)
    return client




@router.get('/client/{id}', response_model=ShowClient, tags=["Client"])
def view_client(id : int, db: Session = Depends(get_db)):
    client_id = crud.client_by_id(id=id, db=db)
    return client_id

@router.put('/client/{id}', tags=["Client"])
def update_client(id : int, client: ShowClient, db: Session = Depends(get_db)):
    client_id = crud.update_client_by_id(id=id, client=client, db=db)
    if client_id is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Job with id {id} does not exist")
    return {"detail":"Successful"}

@router.delete('/client/{id}/delete', tags=["Client"])
def delete_client(id:int, db: Session = Depends(get_db)):
    message = crud.delete_client_by_id(id=id, db=db)
    if not message:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Job with id {id} does not exist")
    return {"detail":"Successful"}