from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
import models
from database import engine, SessionLocal


router = APIRouter()

def get_db():
    db = None
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()



from fastapi import FastAPI, UploadFile, File
import time
import os
import crud
from schemas.invoice import CreateInvoice, ShowInvoice, AllInvoices, NewInvoice


@router.post("/uploadinvoice/", tags=["Invoice"])
async def receive_file2( clientid: int, invoiceid: int, file: UploadFile = File(...), db: Session = Depends(get_db)):

    dir_path = os.path.dirname(os.path.realpath(__file__))
    filename = f'{dir_path}/uploads/{time.time()}-{file.filename}'
    f = open(f'{filename}', 'wb')
    content = await file.read()
    f.write(content)
    invoice2 = crud.create_new_invoice2(clientid=clientid,invoiceid=invoiceid,filename=filename, db=db)
    return invoice2


@router.get('/all', tags=["Invoice"])
def all_invoices(db: Session = Depends(get_db)):
    return db.query(models.InvoiceInfo).all()





@router.post('/create-invoice', response_model=ShowInvoice, tags=["Invoice"])
async def create_invoice(invoice : CreateInvoice, db: Session = Depends(get_db)):

    # dir_path = os.path.dirname(os.path.realpath(__file__))
    # filename = f'{dir_path}/uploads/{time.time()}-{file.filename}'
    # f = open(f'{filename}', 'wb')
    # content = await file.read()
    # f.write(content)

    invoice = crud.create_new_invoice(invoice=invoice, db=db)
    return invoice




@router.get('/invoice/{id}', response_model=ShowInvoice, tags=["Invoice"])
def view_invoice(id : int, db: Session = Depends(get_db)):
    invoice_id = crud.invoice_by_id(id=id, db=db)
    return invoice_id

@router.put('/invoice/{id}', tags=["Invoice"])
def update_invoice(id : int, invoice: ShowInvoice, db: Session = Depends(get_db)):
    invoice_id = crud.update_invoice_by_id(id=id, invoice=invoice, db=db)
    # if invoice_id is None:
    #     raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
    #                         detail="Job with id {id} does not exist")
    return invoice_id

@router.delete('/invoice/{id}/delete', tags=["Invoice"])
def delete_invoice(id:int, db: Session = Depends(get_db)):
    message = crud.delete_invoice_by_id(id=id, db=db)
    # if not message:
    #     raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
    #                         detail="Job with id {id} does not exist")
    return message

