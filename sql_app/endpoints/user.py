
from schemas import invoice, client, sch_user, vendors
import crud
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
import models
from database import engine, SessionLocal

router = APIRouter()

def get_db():
    db = None
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


#### USER

@router.get('/all', tags=["User"])
def all_users(db: Session = Depends(get_db)):
    return db.query(models.UserInfo).all()


@router.post("/add-new", response_model=sch_user.UserInfo, tags=["User"])
def create_user(user: sch_user.UserCreate, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_username(db, username=user.username)
    if db_user:
        raise HTTPException(status_code=400, detail="Username already registered")
    return crud.create_user(db=db, user=user)



@router.get('/{id}', response_model=sch_user.UserInfo, tags=["User"])
def view_user(id : int, db: Session = Depends(get_db)):
    user_id = crud.user_by_id(id=id, db=db)
    return user_id



@router.put('/{id}', tags=["User"])
def update_user(id : int, user: sch_user.UserInfo, db: Session = Depends(get_db)):
    user_id = crud.update_user_by_id(id=id, db=db)

    return user_id
    print(user_id)

@router.delete('/delete/{id}', tags=["User"])
def delete_user(id:int, db: Session = Depends(get_db)):
    message = crud.delete_user_by_id(id=id, db=db)

    # return {"detail":"Successful"}
    return message

