# VENDORS
###########
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
import models
from database import engine, SessionLocal


router = APIRouter()

def get_db():
    db = None
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


from schemas.vendors import CreateVendor, ShowVendor
import crud


@router.get('/all', tags=["Vendor"])
def all_vendors(db: Session = Depends(get_db)):
    return db.query(models.Vendor).all()

@router.post('/create-vendor', response_model=ShowVendor, tags=["Vendor"])
def create_vendor(vendor : CreateVendor,  db: Session = Depends(get_db)):
    return crud.create_new_vendor(db=db, vendor=vendor)



@router.get('/{id}', response_model=ShowVendor, tags=["Vendor"])
def view_vendor(id : int, db: Session = Depends(get_db)):
    vendor_id = crud.vendor_by_id(id=id, db=db)
    return vendor_id

@router.put('/{id}', tags=["Vendor"])
def update_vendor(id : int, vendor: ShowVendor, db: Session = Depends(get_db)):
    vendor_id = crud.update_vendor_by_id(id=id, vendor=vendor, db=db)

    return vendor_id

@router.delete('/delete/{id}', tags=["Vendor"])
def delete_vendor(id:int, db: Session = Depends(get_db)):
    message = crud.delete_vendor_by_id(id=id, db=db)

    # return {"detail":"Successful"}
    return message

###########