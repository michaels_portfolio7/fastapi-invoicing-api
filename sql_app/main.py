import uvicorn
from sqlalchemy.orm import Session
from fastapi import Depends, FastAPI, HTTPException, File, UploadFile
import models
import datetime

import crud
from database import engine, SessionLocal
from schemas import invoice, client, sch_user, vendors


models.Base.metadata.create_all(bind=engine)

ACCESS_TOKEN_EXPIRE_MINUTES = 30

app = FastAPI(
    title='GVR App',
    description='Hello World.'
)

from fastapi.middleware.cors import CORSMiddleware
origins = [
"http://localhost",
"http://localhost:3000",
]
app.add_middleware(
CORSMiddleware,
allow_origins=origins,
allow_credentials=True,
allow_methods=["*"],
allow_headers=["*"],
)

# Dependency


def get_db():
    db = None
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()

#####
# ROUTES

# Auth
from endpoints import auth
# from app_utils import has_access
#
# PROTECTED = [Depends(has_access)]

app.include_router(
    auth.router,
    prefix="/auth",
    dependencies=""
)

# Vendor
from endpoints import vendor
from app_utils import has_access
#
PROTECTED = [Depends(has_access)]

app.include_router(
    vendor.router,
    prefix="/vendor",
    dependencies=PROTECTED
)

# Invoices
from endpoints import invoice
# from app_utils import has_access
#
# PROTECTED = [Depends(has_access)]

app.include_router(
    invoice.router,
    prefix="/invoice",
    dependencies=""
)



# Users
from endpoints import user
from app_utils import has_access
#
PROTECTED = [Depends(has_access)]

app.include_router(
    user.router,
    prefix="/user",
    dependencies=PROTECTED
)




# Clients
from endpoints import client
from app_utils import has_access
#
PROTECTED = [Depends(has_access)]

app.include_router(
    client.router,
    prefix="/client",
    dependencies=PROTECTED
)












if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=2087)