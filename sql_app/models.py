from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Date, Float
from sql_app.database import Base
from sqlalchemy.orm import relationship
import datetime
from datetime import datetime
from typing import TypedDict
# from sqlalchemy_utils import URLType





class UserInfo(Base):
    __tablename__ = "user_info"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True)
    password = Column(String)
    fullname = Column(String, unique=True)

    # FOREIGN KEY
    user_owner_id = Column(Integer)



class InvoiceDict(TypedDict):
    inv_gvr_code: str

class InvoiceInfo(Base):
    __tablename__ = "invoice_info"
    id = Column(Integer, primary_key=True, index=True)
    inv_gvr_code =  Column(String)

    inv_status = Column(String)

    # inv_date = Column(Date)
    inv_issue_date = Column(Date)
    inv_invoice_no = Column(Integer)
    vat_sum = Column(Float)

    inv_item_title = Column(String)
    inv_item_amount = Column(Float)
    inv_item_tax = Column(Float)

    inv_tax_code = Column(Integer)
    inv_net_amount = Column(Float)
    inv_gross_amount = Column(Float)
    inv_vat_amount = Column(Float)
    inv_current = Column(String)

    inv_taxcode_name = Column(String)
    # inv_taxcode_taxvaluepercentage = Column(Integer)
    inv_taxcode_localtaxrate = Column(Integer)
    inv_file_original = Column(String)
    inv_file_new = Column(String)


    inv_owner_id = Column(Integer)







    # def __init__(self, id: int):
    #     self.id = id
    #     self.status = status
    #
    # def __repr__(self) -> int:
    #     return f"<item {self.id}>"

    # @property
    # def serialize(self) -> InvoiceDict:
    #     """
    #     Return item in serializeable format
    #     """
    #     return {"id": self.inv_gvr_code}

### one to one
class Client(Base):
    __tablename__ = "client"

    #COMPANY INFO
    id = Column(Integer, primary_key=True, index=True)
    cust_gvr_number = Column(Integer)
    cust_companyname = Column(String)
    cust_address_streetname = Column(String)
    cust_address_streetnumber = Column(Integer)
    cust_address_zipcode = Column(Integer)
    cust_address_city = Column(String)
    cust_address_state = Column(String)
    cust_address_country = Column(String)

    #LEGAL INFO
    cust_vat_number = Column(Integer)
    cust_regist_number = Column(Integer)
    cust_regit_state = Column(String)
    cust_nace_code = Column(Integer)
    cust_signatory1_fname = Column(String)
    cust_signatory1_lname = Column(String)
    cust_signatory2_fname =  Column(String)
    cust_signatory2_lname = Column(String)
    cust_admin_fname = Column(String)
    cust_admin_lname = Column(String)
    cust_email = Column(String)
    cust_phone = Column(Integer)

    #BANKING INFO
    cust_bank_name = Column(String)
    cust_bic_num = Column(Integer)
    cust_iban_num = Column(Integer)
    cust_vat_countries = Column(String)

    #CONTRACTS
    cust_contract_engagement = Column(String)
    cust_contract_poa = Column(String)

    # FOREIGN KEY (NONE or Has Company Parent)
    parent_id = Column(Integer)



    class Config:
        orm_mode = True

#onee to many
class Vendor(Base):
    __tablename__ = "vendors"
    id = Column(Integer, primary_key=True, index=True)
    vat_number = Column(String)
    vat_country = Column(String)
    legal_name = Column(String)
    registration_number = Column(Integer)
    company_status = Column(String)
    origin_country = Column(String)
    incorp_date = Column(Integer)
    entity_type = Column(String)
    SIC_codes = Column(Integer)
    vendor_address_streetname = Column(String)
    vendor_address_streetnumber = Column(Integer)
    vendor_address_zipcode = Column(Integer)
    vendor_address_state = Column(String)
    vendor_address_country = Column(String)

    # One to many
