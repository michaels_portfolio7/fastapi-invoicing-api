from pydantic import BaseModel
from typing import Optional
from datetime import date,datetime


class Client(BaseModel):


    cust_gvr_number : int
    cust_companyname : str
    cust_address_streetname : str
    cust_address_streetnumber : int
    cust_address_zipcode : int
    cust_address_city : str
    cust_address_state : str
    cust_address_country : str

    cust_vat_number : str
    cust_regist_number : int
    cust_regit_state : str
    cust_nace_code : int
    cust_signatory1_fname : str
    cust_signatory1_lname : str
    cust_signatory2_fname : str
    cust_signatory2_lname : str
    cust_admin_fname : str
    cust_admin_lname : str
    cust_email : str
    cust_phone : int

    cust_bank_name : str
    cust_bic_num : int
    cust_iban_num : int
    cust_vat_countries : str

    cust_contract_engagement : str
    cust_contract_poa : str

    class Config:
        orm_mode = True


class CreateClient(Client):
    cust_gvr_number : int

class ShowClient(Client):
    cust_gvr_number : int


    class Config:
        orm_mode = True