from pydantic import BaseModel
from typing import Optional
from datetime import date,datetime
from enum import Enum


# class InvoiceStatus(str, Enum):
#     processing = 'Processing'
#     low_refund = 'Low Refund'
#     not_refunded = 'Not Refunded'
#     accepted =  'Accepted'
#     submitted = 'Submitted'
#     rejected = 'Rejected'
#     request = 'Request'
#     refunded = 'Refunded'
#     review = 'Review'
class NewInvoice(BaseModel):
    inv_gvr_code: str
    inv_file_original : Optional[str]
    inv_owner_id : Optional[int]


class InvoiceB(BaseModel):
    inv_gvr_code: str

    inv_status : Optional[str]

    # inv_date : Optional[date]

    inv_issue_date : Optional[date]
    inv_invoice_no : Optional[int]
    vat_sum : Optional[float]
    inv_item_title : Optional[str]
    inv_item_amount : Optional[float]
    inv_item_tax : Optional[float]
    inv_tax_code : Optional[int]
    inv_net_amount : Optional[float]
    inv_gross_amount : Optional[float]
    inv_vat_amount : Optional[float]
    inv_current : Optional[str]

    inv_taxcode_name : Optional[str]
    # inv_taxcode_taxvaluepercentage: Optional[int]
    inv_taxcode_localtaxrate : Optional[int]
    inv_file_original : str
    inv_file_new : Optional[str]

    inv_owner_id : Optional[int]

    # THIS CANNOT BE NULL OR 0
    # inv_owner_id1 : Optional[int]
    # inv_vendor_id : Optional[int]


    class Config:
        orm_mode = True


class CreateInvoice(InvoiceB):
    inv_gvr_code: str



    class Config:
        orm_mode = True

class ShowInvoice(InvoiceB):

    class Config:
        orm_mode = True


class AllInvoices(InvoiceB):
    inv_gvr_code: str


    class Config:
        orm_mode = True

