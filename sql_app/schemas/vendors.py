from pydantic import BaseModel
from typing import Optional
from datetime import date,datetime


class Vendor(BaseModel):
    vat_number : Optional[str]
    legal_name : Optional[str]
    registration_number : Optional[int]
    company_status : Optional[str] #change to list
    origin_country : Optional[str]
    incorp_date : Optional[int]
    entity_type : Optional[str] #change to list
    SIC_codes : Optional[int] #Change to list
    vendor_address_streetname : Optional[str]
    vendor_address_streetnumber : Optional[int]
    vendor_address_zipcode : Optional[int]
    vendor_address_state : Optional[str]
    vendor_address_country :Optional[str]

    class Config:
        orm_mode = True


class CreateVendor(Vendor):
    vat_number : int

class ShowVendor(Vendor):
    vat_number : str

    class Config:
        orm_mode = True