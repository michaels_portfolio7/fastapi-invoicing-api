
# USER TABLE
```bash
CREATE TABLE user_info(
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
username VARCHAR(50) NOT NULL,
password VARCHAR(500) NOT NULL,
fullname VARCHAR(50) NOT NULL
);
```

# INVOICE TABLE
```bash
CREATE TABLE invoice_info(
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
inv_status VARCHAR(500) NOT NULL,
inv_issue_data INT(50) NOT NULL,
inv_invoice_no INT(50) NOT NULL,
vat_sum INT(50) NOT NULL,
inv_item_title VARCHAR(500) NOT NULL,
inv_item_amount INT(50) NOT NULL,
inv_item_tax INT(50) NOT NULL,
inv_tax_code INT(50) NOT NULL,
inv_net_amount  INT(50) NOT NULL,
inv_gross_amount INT(50) NOT NULL,
inv_vat_amount  INT(50) NOT NULL,
inv_current VARCHAR(500) NOT NULL,
inv_taxcode_name VARCHAR(500) NOT NULL,
inv_taxcode_taxvaluepercentage VARCHAR(500) NOT NULL,
inv_taxcode_localtaxrate  INT(50) NOT NULL
);

```

# CLIENT TABLE
# VENDOR TABLE


```bazaar
DROP TABLE table_name;
```

```bazaar
ALTER TABLE candidates (
ADD COLUMN inv_file_original ,
ADD COLUMN inv_file_new VARCHAR(255),
ADD COLUMN inv_owner_id INT(50);
CONSTRAINT fkName FOREIGN KEY(client.id) 
    REFERENCES client(id);
    );
```

    inv_file_original = Column(String)
   ç = Column(String)


    inv_owner_id = Column(Integer, ForeignKey("client.id"))




# INVOICE TABLE
```bash
CREATE TABLE invoice_info(
    id INT NOT NULL,
    PRIMARY KEY (id)

    inv_status VARCHAR(500),
    inv_issue_data INT(50),
    inv_invoice_no INT(50),
    vat_sum INT(50),
    inv_item_title VARCHAR(500),
    inv_item_amount INT(50),
    inv_item_tax INT(50),
    inv_tax_code INT(50),
    inv_net_amount  INT(50),
    inv_gross_amount INT(50),
    inv_vat_amount  INT(50),
    inv_current VARCHAR(500),
    inv_taxcode_name VARCHAR(500),
    inv_taxcode_taxvaluepercentage VARCHAR(500),
    inv_taxcode_localtaxrate  INT(50)
    inv_file_original VARCHAR(500),
    inv_file_new VARCHAR(500),
    inv_owner_id INT,    
    
    INDEX client_inv (inv_owner_id),
    FOREIGN KEY (inv_owner_id)
        REFERENCES client(id)
        ON DELETE CASCADE
);

```

# ADD FOREIGN KEY DB
```
ALTER TABLE table_name
ADD COLUMN column_definition INT,
ADD INDEX client_inv (inv_owner_id),
ADD FOREIGN KEY (inv_owner_id)
```

```bazaar
ALTER TABLE invoice_info
ADD COLUMN inv_vendor_id INT,
ADD INDEX vendor_inv (inv_vendor_id),
ADD FOREIGN KEY (inv_vendor_id)
REFERENCES `vendors`(`id`);


set foreign_key_checks=0;
ALTER TABLE invoice_info
ADD FOREIGN KEY(inv_vendor_id) 
REFERENCES vendors(id) 
set foreign_key_checks=1;

```

CREATE TABLE client (
    id INT NOT NULL,
    PRIMARY KEY (id),
    cust_gvr_number INT,
    cust_companyname VARCHAR(500),
    cust_address_streetname VARCHAR(500),
    cust_address_streetnumber INT,
    cust_address_zipcode INT,
    cust_address_city VARCHAR(500),
    cust_address_state VARCHAR(500),
    cust_address_country VARCHAR(500),
    cust_vat_number INT,
    cust_regist_number INT,
    cust_regit_state VARCHAR(500),
    cust_nace_code INT,
    cust_signatory1_fname VARCHAR(500),
    cust_signatory1_lname VARCHAR(500),
    cust_signatory2_fname  VARCHAR(500),
    cust_signatory2_lname VARCHAR(500),
    cust_admin_fname VARCHAR(500),
    cust_admin_lname VARCHAR(500),
    cust_email VARCHAR(500),
    cust_phone INT,
    cust_bank_name VARCHAR(500),
    cust_bic_num INT,
    cust_iban_num INT,
    cust_vat_countries VARCHAR(500),
    cust_contract_engagement VARCHAR(500),
    cust_contract_poa VARCHAR(500),

)

    