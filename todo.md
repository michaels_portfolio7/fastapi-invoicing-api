# Pending Todo

---
-[X]  Connect to DB server
 - [X] Put in SQL Script 
-[ ]  Async vs 
-[ ] Implement Async
-[ ] Complete Models
- [] Fix FastAPI info
---

### Useful Info

Server IP: http://81.24.10.xxx:8091/


# Project Notes

### SSH Creds
```bazaar
ssh -i keypair-ml.cer ubuntu@2a05:6cc1::3d5
```
*** This keypair is located in localenv DL folder
### Docker Compose File
```bazaar
  db:
    image: mysql:5.7
    container_name: db
    environment:
      MYSQL_ROOT_PASSWORD:  
      MYSQL_DATABASE: app_db
      MYSQL_USER: db_user
      MYSQL_PASSWORD: 
    ports:
      - "8092:3306"
    volumes:
      - dbdata:/var/lib/mysql
  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    container_name: pma
    links:
      - db
    environment:
      PMA_HOST: db
      PMA_PORT: 3306
      PMA_ARBITRARY: 1
    restart: always
    ports:
      - 8091:80
```


### SQL Dump

```bash
CREATE DATABASE restapi;
USE restapi;
```

```bash
CREATE TABLE user_info(
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
username VARCHAR(50) NOT NULL,
password VARCHAR(500) NOT NULL,
fullname VARCHAR(50) NOT NULL
);
```


```bash
CREATE TABLE invoice_info(
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
inv_status VARCHAR(500) NOT NULL,
inv_issue_data INT(50) NOT NULL,
inv_invoice_no INT(50) NOT NULL,
vat_sum INT(50) NOT NULL,
inv_item_title VARCHAR(500) NOT NULL,
inv_item_amount INT(50) NOT NULL,
inv_item_tax INT(50) NOT NULL,
inv_tax_code INT(50) NOT NULL,
inv_net_amount  INT(50) NOT NULL,
inv_gross_amount INT(50) NOT NULL,
inv_vat_amount  INT(50) NOT NULL,
inv_current VARCHAR(500) NOT NULL,
inv_taxcode_name VARCHAR(500) NOT NULL,
inv_taxcode_taxvaluepercentage VARCHAR(500) NOT NULL,
inv_taxcode_localtaxrate  INT(50) NOT NULL
);

```

```bazaar
DROP TABLE table_name;
```

